// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.jetbrainsKotlinAndroid) apply false
}
buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath ("org.jetbrains.kotlin:kotlin-gradle-plugin")
        classpath ("com.android.tools.build:gradle:8.3.2")
        classpath("com.jfrog.bintray.gradle:gradle-bintray-plugin:1.7.3")
        classpath ("com.android.tools.build:gradle:8.2.2")
        classpath ("com.google.gms:google-services:4.4.0")
    }
}