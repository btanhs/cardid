pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
    repositories {
        maven {
            url = uri ("https://download2.dynamsoft.com/maven/aar")
        }
    }
    repositories {
        jcenter()
        maven { url = uri( "https://jitpack.io") }
    }

}

rootProject.name = "CardID"
include(":app")
 