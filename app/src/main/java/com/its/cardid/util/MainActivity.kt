package com.its.cardid.util

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.google.android.material.snackbar.Snackbar
import com.its.cardid.R
import com.its.cardid.nfc.ImageUtil
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import net.sf.scuba.smartcards.CardService
import org.apache.commons.io.IOUtils
import org.bouncycastle.asn1.ASN1InputStream
import org.bouncycastle.asn1.ASN1Primitive
import org.bouncycastle.asn1.ASN1Sequence
import org.bouncycastle.asn1.ASN1Set
import org.bouncycastle.asn1.x509.Certificate
import org.ejbca.cvc.exception.ParseException
import org.jmrtd.BACKey
import org.jmrtd.BACKeySpec
import org.jmrtd.PassportService
import org.jmrtd.lds.CardAccessFile
import org.jmrtd.lds.ChipAuthenticationPublicKeyInfo
import org.jmrtd.lds.PACEInfo
import org.jmrtd.lds.SODFile
import org.jmrtd.lds.SecurityInfo
import org.jmrtd.lds.icao.DG14File
import org.jmrtd.lds.icao.DG1File
import org.jmrtd.lds.icao.DG2File
import org.jmrtd.lds.iso19794.FaceImageInfo
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.io.InputStream
import java.security.KeyStore
import java.security.MessageDigest
import java.security.Signature
import java.security.cert.CertPathValidator
import java.security.cert.CertificateFactory
import java.security.cert.PKIXParameters
import java.security.cert.X509Certificate
import java.security.spec.MGF1ParameterSpec
import java.security.spec.PSSParameterSpec
import java.util.ArrayList
import java.util.Arrays
import java.util.Locale
abstract class MainActivity : AppCompatActivity() {
    public lateinit var cardIDView: EditText
    public lateinit var expirationDateView: EditText
    public lateinit var birthDateView: EditText
    private lateinit var btn_info: Button
    private lateinit var btnAutomatic: Button
    private var cardIDFromIntent = false
    private lateinit var mainLayout: View
    private lateinit var loadingLayout: View
    private var encodePhotoToBase64 = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        getIntents()
        encodePhotoToBase64 = intent.getBooleanExtra("photoAsBase64", false)

        init()
        btnAutomatic.setOnClickListener {
            var intent= Intent(this, CameraMRZ::class.java);
            startActivity(intent)
        }
        cardIDView.setText(preferences.getString(KEY_CARD_ID, null))
        expirationDateView.setText(preferences.getString(KEY_EXPIRATION_DATE, null))
        birthDateView.setText(preferences.getString(KEY_BIRTH_DATE, null))
        cardIDView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
                    .edit().putString(KEY_CARD_ID, s.toString()).apply()
            }
        })

        expirationDateView.setOnClickListener {
            val c = loadDate(expirationDateView)
            val dialog = DatePickerDialog.newInstance(
                { _, year, monthOfYear, dayOfMonth ->
                    saveDate(
                        expirationDateView,
                        year,
                        monthOfYear,
                        dayOfMonth,
                        KEY_EXPIRATION_DATE,
                    )
                },
                c[Calendar.YEAR],
                c[Calendar.MONTH],
                c[Calendar.DAY_OF_MONTH],
            )
            dialog.showYearPickerFirst(true)
            fragmentManager.beginTransaction().add(dialog, null).commit()
        }

        birthDateView.setOnClickListener {
            val c = loadDate(birthDateView)
            val dialog = DatePickerDialog.newInstance(
                { _, year, monthOfYear, dayOfMonth ->
                    saveDate(birthDateView, year, monthOfYear, dayOfMonth, KEY_BIRTH_DATE)
                },
                c[Calendar.YEAR],
                c[Calendar.MONTH],
                c[Calendar.DAY_OF_MONTH],
            )
            dialog.showYearPickerFirst(true)
            fragmentManager.beginTransaction().add(dialog, null).commit()
        }
        btn_info.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, AbMrzInfoActivity::class.java)

            var cardid = cardIDView.text.toString();
            var expdate = expirationDateView.text.toString();
            var birthdate = birthDateView.text.toString();
            Log.i(TAG, "onCreate: $cardid $expdate $birthdate")
            intent.putExtra(KEY_CARD_ID, cardid)
            intent.putExtra(KEY_BIRTH_DATE, birthdate)
            intent.putExtra(KEY_EXPIRATION_DATE, expdate)
            startActivity(intent)
        })
    }


    fun init(){
        btnAutomatic = findViewById(R.id.btnAutomatic)
        cardIDView = findViewById(R.id.input_idcard_number)
        expirationDateView = findViewById(R.id.input_expiration_date)
        birthDateView = findViewById(R.id.input_birthday)
        mainLayout = findViewById(R.id.main_layout)
        btn_info = findViewById(R.id.btn_info)

    }
    fun getIntents(){
        val dateOfBirth = intent.getStringExtra("dateOfBirth")
        val dateOfExpiry = intent.getStringExtra("dateOfExpiry")
        val cardId = intent.getStringExtra("passportNumber")
        if (dateOfBirth != null){
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString(KEY_BIRTH_DATE,dateOfBirth ).apply()
        }
        if (dateOfExpiry != null){
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString(KEY_EXPIRATION_DATE,dateOfExpiry ).apply()
        }
        if (cardId != null){
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString(KEY_CARD_ID,cardId ).apply()
            cardIDFromIntent = true
        }
    }
    private fun loadDate(editText: EditText): Calendar {
        val calendar = Calendar.getInstance()
        if (editText.text.isNotEmpty()) {
            try {
                calendar.timeInMillis = SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(editText.text.toString())!!.time
            } catch (e: ParseException) {
                Log.w(MainActivity::class.java.simpleName, e)
            }
        }
        return calendar
    }
    private fun saveDate(editText: EditText, year: Int, monthOfYear: Int, dayOfMonth: Int, preferenceKey: String) {
        val value = String.format(Locale.US, "%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth)
        PreferenceManager.getDefaultSharedPreferences(this)
            .edit().putString(preferenceKey, value).apply()
        editText.setText(value)
    }
    companion object {
        private val TAG = MainActivity::class.java.simpleName
        const val KEY_CARD_ID = "cardID"
        const val KEY_EXPIRATION_DATE = "expirationDate"
        const val KEY_BIRTH_DATE = "dateOfBirth"
    }
}