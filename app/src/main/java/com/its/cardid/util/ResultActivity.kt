package com.its.cardid.util

import android.os.Bundle
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import android.widget.ImageView

import com.its.cardid.R

class ResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_result)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        findViewById<TextView>(R.id.textViewFullName).text = intent.getStringExtra(KEY_LAST_NAME) +" "+ " " +intent.getStringExtra(KEY_FIRST_NAME)
        findViewById<TextView>(R.id.textViewGender).text = intent.getStringExtra(KEY_GENDER)
        findViewById<TextView>(R.id.textViewState).text = intent.getStringExtra(KEY_STATE)
        findViewById<TextView>(R.id.textViewNational).text = intent.getStringExtra(KEY_NATIONALITY)
        findViewById<TextView>(R.id.textViewBirthday).text = intent.getStringExtra(KEY_PASSIVE_AUTH)
        findViewById<TextView>(R.id.textViewExpirationDate).text = intent.getStringExtra(KEY_CHIP_AUTH)
        if (intent.hasExtra(KEY_PHOTO)) {
            @Suppress("DEPRECATION")
            findViewById<ImageView>(R.id.imageViewIDCard).setImageBitmap(intent.getParcelableExtra(KEY_PHOTO))
        }
    }
    companion object {
        const val KEY_FIRST_NAME = "firstName"
        const val KEY_LAST_NAME = "lastName"
        const val KEY_GENDER = "gender"
        const val KEY_STATE = "state"
        const val KEY_NATIONALITY = "nationality"
        const val KEY_PHOTO = "photo"
        const val KEY_PHOTO_BASE64 = "photoBase64"
        const val KEY_PASSIVE_AUTH = "passiveAuth"
        const val KEY_CHIP_AUTH = "chipAuth"
    }
}