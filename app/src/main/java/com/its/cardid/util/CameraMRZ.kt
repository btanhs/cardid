package com.its.cardid.util

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import com.its.cardid.mrz.MRZAnalyzer
import com.its.cardid.theme.MRZScannerTheme

class CameraMRZ  : ComponentActivity(){
    private lateinit var CardID : String;
    private lateinit var birthday : String;
    private lateinit var expirationDay : String;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MRZScannerTheme {
                var code by remember {
                    mutableStateOf("")
                }
                val context = LocalContext.current
                val lifecycleOwner = LocalLifecycleOwner.current
                val cameraProviderFuture = remember {
                    ProcessCameraProvider.getInstance(context)
                }
                var hasCamPermission by remember {
                    mutableStateOf(
                        ContextCompat.checkSelfPermission(
                            context,
                            Manifest.permission.CAMERA
                        ) == PackageManager.PERMISSION_GRANTED
                    )
                }
                val launcher = rememberLauncherForActivityResult(
                    contract = ActivityResultContracts.RequestPermission(),
                    onResult = { granted ->
                        hasCamPermission = granted
                    }
                )
                LaunchedEffect(key1 = true) {
                    launcher.launch(Manifest.permission.CAMERA)
                }
                Column(
                    modifier = Modifier.fillMaxSize()
                ) {
                    if (hasCamPermission) {
                        AndroidView(
                            factory = { context ->
                                val previewView = PreviewView(context)
                                val preview = Preview.Builder().build()
                                val selector = CameraSelector.Builder()
                                    .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                                    .build()
                                preview.setSurfaceProvider(previewView.surfaceProvider)
                                val imageAnalysis = ImageAnalysis.Builder()
                                    .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                                    .build()
                                imageAnalysis.setAnalyzer(
                                    ContextCompat.getMainExecutor(context),
                                    MRZAnalyzer({results->
                                        run {
                                            val sb = StringBuilder()
                                            if (results.size == 1) {
                                                val result = results.get(0)
                                                if (result.lineResults.size>=2) {
                                                    for (lineResult in result.lineResults) {
                                                        sb.append(lineResult.text)
                                                        sb.append("\n")
                                                    }
                                                }
                                            }
                                            code = sb.toString()
                                            setCode(code)

                                        }
                                    },context)
                                )
                                try {
                                    cameraProviderFuture.get().bindToLifecycle(
                                        lifecycleOwner,
                                        selector,
                                        preview,
                                        imageAnalysis
                                    )
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                previewView
                            },
                            modifier = Modifier
                                .weight(1f)
                                .padding(bottom = 25.dp)
                        )
                        Text(
                            text = code,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(32.dp)
                        )
                    }
                }
            }
        }
    }
    private val TAG ="Camera MRZ";
    private fun setCode (code:String) {
        CardID = code.substring(5, 14)

        var expirateYear = "20" + code.substring(39,41)

        expirationDay =expirateYear +"-"+ code.substring(41,43)+"-"+ code.substring(43,45)
        var birthYear = code.substring(31,33)
        var intBirthYear = birthYear.toInt()
        if (intBirthYear >= 50 && intBirthYear <= 99) {
            intBirthYear = intBirthYear + 1900
        }else{
            intBirthYear = intBirthYear + 2000
        }
        birthYear = intBirthYear.toString()


        birthday = birthYear +"-" + code.substring(33,35)+"-" + code.substring(35,37)
        Log.i(TAG, "setCode: $CardID $expirationDay  $birthday")
        setIntent()
    }
    private fun setIntent(){
        var intent = Intent(this, AbMrzInfoActivity::class.java)
        intent.putExtra(MainActivity.KEY_CARD_ID,CardID);
        intent.putExtra(MainActivity.KEY_EXPIRATION_DATE,expirationDay);
        intent.putExtra(MainActivity.KEY_BIRTH_DATE,birthday)
        startActivity(intent)
    }
}