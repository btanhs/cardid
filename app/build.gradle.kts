plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.jetbrainsKotlinAndroid)
}

android {
    namespace = "com.its.cardid"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.its.cardid"
        minSdk = 28
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    flavorDimensions += "default"
    productFlavors{
        create("regular"){
            isDefault = true
        }
        create("google")
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    packagingOptions {
        resources {
            excludes += "META-INF/LICENSE"
            excludes += "META-INF/NOTICE"
        }
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)

    implementation("androidx.camera:camera-core:1.3.3")
    implementation("androidx.camera:camera-camera2:1.3.3")
    implementation("androidx.camera:camera-extensions:1.3.3")
    implementation("androidx.camera:camera-lifecycle:1.3.3")
    implementation("androidx.camera:camera-extensions:1.3.3")

    implementation("com.dynamsoft:dynamsoftlabelrecognizer:2.2.20")

    implementation("androidx.compose.ui:ui-graphics:1.6.6")
    implementation("androidx.compose.ui:ui:1.6.6")
    implementation("androidx.compose.ui:ui-tooling-preview:1.6.6")
    implementation("androidx.compose.material3:material3:1.2.1")
    implementation ("androidx.core:core-ktx:1.13.0")
    implementation ("androidx.lifecycle:lifecycle-runtime-ktx:2.7.0")
    implementation ("androidx.activity:activity-compose:1.7.2")
    implementation ("androidx.compose:compose-bom:2023.03.00")


    implementation("org.jetbrains.kotlin:kotlin-reflect:1.8.10")

    implementation ("com.gemalto.jp2:jp2-android:1.0.3")
    implementation ("androidx.multidex:multidex:2.0.1")
    implementation ("com.wdullaer:materialdatetimepicker:3.5.2")
    implementation ("org.jmrtd:jmrtd:0.7.18")
    implementation ("net.sf.scuba:scuba-sc-android:0.0.18")
    implementation ("com.madgag.spongycastle:prov:1.54.0.0")
    implementation ("com.github.mhshams:jnbis:1.1.0")
    implementation ("org.bouncycastle:bcpkix-jdk15on:1.65") // do not update
    implementation ("commons-io:commons-io:2.11.0")
    implementation ("androidx.preference:preference:1.2.0")


    implementation("com.google.firebase:firebase-bom:32.5.0")
    implementation("com.google.firebase:firebase-analytics-ktx:21.6.2")
    implementation("com.google.firebase:firebase-crashlytics:18.6.4")
    implementation ("com.google.android.gms:play-services-ads:22.5.0")
    implementation ("com.google.android.play:review-ktx:2.0.1")
    implementation(libs.androidx.camera.view)
    implementation(libs.androidx.constraintlayout)



    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
}